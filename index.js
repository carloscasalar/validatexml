// Validates XML file against a XSD schema. Inspired by http://stackoverflow.com/a/14871585/840635
'use strict';
var fs = require('fs');
var _ = require('lodash');
var libXml = require('libxmljs');
var validations = require('./validations').validations;

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
var MESSAGES = {
  fileIsValid: _.template('Validation result of "{{xmlFile}}" against "{{xsdFile}}": VALID\n\n'),
  fileIsInvalid:
  _.template('Validation result of "{{xmlFile}}" against "{{xsdFile}}": INVALID\n====\n{{errorMsg}}\n====\n\n')
};

if (validations && validations instanceof Array) {
  validations.forEach(validate);
} else {
  console.error('validations config file invalid');
}

/**
 * Validate an XML against a XSD
 * @param {object} validation Object containing the XML and XSD path
 * @param {string} validation.xsd Path of the XSD file
 * @param {string} validation.xml Path of the XML file
 */
function validate(validation) {
  var xsdDoc = parseXml(validation.xsd);
  var xmlDoc = parseXml(validation.xml);
  var isValid = xmlDoc.validate(xsdDoc);
  var messageOpts = {
    xmlFile: validation.xml,
    xsdFile: validation.xsd,
    errorMsg: xmlDoc.validationErrors
  };

  if (isValid) {
    console.log(MESSAGES.fileIsValid(messageOpts));
  } else {
    console.error(MESSAGES.fileIsInvalid(messageOpts));
  }
}

/**
 * Parse XML file
 * @param  {string} filePath Path of the XML file to parse
 * @return {Document} Parsed XML file
 */
function parseXml(filePath) {
  var file, doc;
  if (filePath) {
    file = fs.readFileSync(filePath);
  }
  if (file) {
    doc = libXml.parseXmlString(file);
  }
  return doc;
}
